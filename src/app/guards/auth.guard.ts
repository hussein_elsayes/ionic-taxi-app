import { Injectable } from '@angular/core';
import {CanLoad, Route, UrlSegment, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthenticationService } from '../services/authentication.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements  CanLoad{
  constructor(private authService:AuthenticationService, private router: Router){}

  canLoad(route: Route, segments: UrlSegment[]): Observable<boolean> | Promise<boolean> | boolean
  {
    this.authService.getToken().then(token => {
      if(!token){
        return false;
      }
    })
    return true;
  }

}
