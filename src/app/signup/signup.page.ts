import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { NavController, LoadingController, AlertController } from '@ionic/angular';
import { RequestsService } from '../services/requests.service';
import { Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { AuthenticationService } from '../services/authentication.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: ['./signup.page.scss'],
})
export class SignupPage implements OnInit {

  constructor( private fb: FormBuilder, private navCtrl : NavController,
     private alertCtrl : AlertController,private loadingCtrl : LoadingController,
      private requestsService : RequestsService,private router: Router
      ,private authService:AuthenticationService) { }

  signupForm = this.fb.group(
    {
      name : ['',[Validators.required]],
      mobileNo : ['',[Validators.required,Validators.pattern(/^[5][0-9]{8}$/)]],
      email : ['',[Validators.required,Validators.pattern(/^([_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*(\.[a-zA-Z]{1,6}))?$/)]],
      password : ['',[Validators.required]],
      confirmPassword : ['',[Validators.required]]
    }
  );
  ngOnInit() {
  }

  onSubmit(signupForm){
    console.log(signupForm);
    this.loadingCtrl.create({message : 'يرجى الانتظار '}).then(returnedElement => {
      returnedElement.present();
      this.requestsService.createUserRequest(signupForm.value).subscribe(resp => {
        returnedElement.dismiss()
        this.authService.logout();
        this.alertCtrl.create({message : "تم انشاء المستخدم بنجاح , سيصلك كود التفعيل عبر الايميل المسجل (ملحوظة : من الممكن ان يتواجد الايميل في قسم الايميلات الغير مرغوب بها في بريدك الالكتروني )"}).then(alertElement => {
          alertElement.present();
        })
      }, (error : HttpErrorResponse) => {
        returnedElement.dismiss();
        if(error.status == 409){
          this.alertCtrl.create({message : "يجب تطابق كلمة السر"}).then(alertElement => {
            alertElement.present();
          })
        }else if(error.status == 423){
          this.alertCtrl.create({message : "هذا المستخدم موجود مسبقا"}).then(alertElement => {
            alertElement.present();
          })
        }
        else{
          this.alertCtrl.create({message : "خطأ في انشاء المستخدم , يرجى المحاولة لاحقا"}).then(alertElement => {
            alertElement.present();
          })
        }
      });
    })
    
  }

  close(){
    this.navCtrl.pop();
  }

}
