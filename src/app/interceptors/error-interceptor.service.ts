import { Injectable } from '@angular/core';
import {HttpInterceptor,HttpRequest,HttpHandler, HttpEvent, HttpErrorResponse} from '@angular/common/http'
import { catchError } from 'rxjs/operators';
import { Router } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { AuthenticationService } from '../services/authentication.service';
@Injectable({
  providedIn: 'root'
})
export class ErrorInterceptorService implements HttpInterceptor{

  constructor(private _router :Router,private authService : AuthenticationService) { }

  intercept( request : HttpRequest<any>, next : HttpHandler) : Observable<HttpEvent<any>>
  {
  return next.handle(request).pipe
    (
      catchError(err => {
            if(err.status === 401 || err.status === 403)
            {
              console.log('error is : unAuthorized !')
              this.authService.logout();
            }
            console.log('error is : ' + err.status)          
            let error = err.error.message || err.statusText;
            return throwError(error);
          }
        )
    )
  }
}
