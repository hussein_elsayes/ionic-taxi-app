import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpResponse,
  HttpErrorResponse
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { AuthenticationService } from '../services/authentication.service';

@Injectable({
  providedIn: 'root'
})
export class TokenInterceptor implements HttpInterceptor{

  constructor(private authService : AuthenticationService) { }

  intercept(req : HttpRequest<any> , next : HttpHandler) : Observable<HttpEvent<any>>
  {
    console.log('token interceptor hit !');
    let token = this.authService.getToken();
    req = req.clone({setHeaders : {
      Accept: `application/json`,
      'Content-Type': `application/json`,
      Authorization : 'Bearer ' + token
    }
    });
    console.log(req);
    return next.handle(req).pipe(
      catchError((error: HttpErrorResponse) => {
        return throwError(error);
      }));
  }

}
