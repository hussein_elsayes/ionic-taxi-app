import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { NavController, LoadingController, AlertController } from '@ionic/angular';
import { AuthenticationService } from '../services/authentication.service';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { TokenModel } from '../interfaces/TokenModel';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
isLoading = false;
showValidateButton =false;
  constructor( private fb: FormBuilder,
    private storage : Storage,
     private router: Router,
      private navCtrl : NavController,
       private loadingCtrl : LoadingController,
        private authService : AuthenticationService ,
         private alertCtrl : AlertController) { }
  loginForm = this.fb.group(
    {
      username : ['',[Validators.required]],
      password : ['',[Validators.required]]
    }
  );
  ngOnInit() {
  }

testLogin(){
  this.navCtrl.navigateForward('/admin/drivers')
}

  onSubmit(loginForm){
    console.log(loginForm);
    this.loadingCtrl.create({message : 'تسجيل الدخول'}).then(returnedElement => {
      returnedElement.present();
      this.authService.login(loginForm.value).subscribe((resp : HttpResponse<TokenModel>) => {
        returnedElement.dismiss()
        this.storage.set('authToken', resp).then(returnedToken => {
          console.log(returnedToken);
          this.router.navigateByUrl('/home');
          //location.reload();
        })
      }, (error : HttpErrorResponse) => {
        returnedElement.dismiss();
        console.log(error);
        if(error.status == 451){
          this.alertCtrl.create({message : "هذا المستخدم غير مفعل , يرجى التفعيل بالضغط على الرابط في الاسفل"}).then(alertElement => {
            alertElement.present();
          })
          this.showValidateButton = true;
        }else{
          this.alertCtrl.create({message : "خطأ في تسجيل الدخول , يرجى المحاولة لاحقا"}).then(alertElement => {
            alertElement.present();
          })
        }
      });
    })
  }

  showSignup(){
    this.router.navigateByUrl('/signup');
  }

  goToValidate(){
    this.router.navigateByUrl('/validate-account');
  }

}
