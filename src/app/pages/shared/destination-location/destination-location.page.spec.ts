import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DestinationLocationPage } from './destination-location.page';

describe('DestinationLocationPage', () => {
  let component: DestinationLocationPage;
  let fixture: ComponentFixture<DestinationLocationPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DestinationLocationPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DestinationLocationPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
