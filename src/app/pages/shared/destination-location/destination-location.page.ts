import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Storage } from '@ionic/storage';
import {
  GoogleMaps,
  GoogleMap,
  Geocoder,
  BaseArrayClass,
  GeocoderResult,
  Marker,
  LatLng,
  Polyline
} from '@ionic-native/google-maps';
import { LoadingController, Platform, NavController, AlertController } from '@ionic/angular';
import { GoogleMapsEvent } from '@ionic-native/google-maps/ngx';
import { LocationService } from 'src/app/services/location.service';
import { Georoute } from 'src/app/interfaces/GeoRoute';
import { ActivatedRoute } from '@angular/router';
import { NavigationOptions } from '@ionic/angular/dist/providers/nav-controller';

import { Geolocation } from '@ionic-native/geolocation/ngx';


@Component({
  selector: 'app-destination-location',
  templateUrl: './destination-location.page.html',
  styleUrls: ['./destination-location.page.scss'],
})
export class DestinationLocationPage implements OnInit {
  map1: GoogleMap;
  loading: any;
  sourceMarker:Marker;
  destinationMarker:Marker;
  sourceLat;
  sourceLng;
  SrcDestRoute:Georoute;
  routeDistance;



  @ViewChild('search_address', {static : false}) search_address: ElementRef;
  
  constructor(public loadingCtrl: LoadingController,
     private platform: Platform, private storage : Storage,
      private navCtrl : NavController, private locationService : LocationService ,
      private activatedRoute: ActivatedRoute, private geolocation: Geolocation,
      private alertCtrl : AlertController) { }
  
  async ngOnInit() {
      await this.platform.ready();
      await this.initiateMap();
  }


  async initiateMap()
  {
    this.loadingCtrl.create({message : 'يرجى الانتظار '}).then(returnedElement => {

    //trying to get location from gps
    this.geolocation.getCurrentPosition().then(curLocation => {
      returnedElement.dismiss();
      this.sourceLat = curLocation.coords.latitude;
      this.sourceLng = curLocation.coords.longitude;
      this.map1 = GoogleMaps.create('map_canvas1',{
        camera: {
          target: {
            lat: this.sourceLat,
            lng: this.sourceLng
          },
          zoom: 18,
          tilt: 30
        }
      });

      //add the sourceMarker
      this.addSourceMarker(curLocation.coords.latitude, curLocation.coords.longitude);
      this.addMapClickListener();

    }).catch(error=>{
      returnedElement.dismiss();
      this.alertCtrl.create({message : "خطأ في تحميل الخريطة , يرجى المحاولة لاحقا"}).then(alertElement => {
        alertElement.present();
      })
    })

    });
  }

  addSourceMarker(lat,lng){
    this.sourceMarker = this.map1.addMarkerSync({'position': {lat : lat, lng : lng},'title':  'العنوان'});
    this.sourceMarker.showInfoWindow();
  }

  addMapClickListener(){
    this.map1.on(GoogleMapsEvent.MAP_CLICK).subscribe((response : Array<any>) => {
      this.map1.clear();
      this.addSourceMarker(this.sourceLat, this.sourceLng)
      this.destinationMarker = this.map1.addMarkerSync({
        'position': new LatLng(response[0].lat,response[0].lng),
        'title':  'الوجهة'
      });
      
      this.drawRoute();
      this.destinationMarker.showInfoWindow();
    })
  }



  drawRoute()
  {
    if(this.sourceMarker && this.destinationMarker)
    {
      this.locationService.getGeoRoute(this.sourceMarker.getPosition(), this.destinationMarker.getPosition()).subscribe( (response:any) =>{
        this.routeDistance = response.routes[0].distance / 1000;
        
        this.map1.addMarkerSync({
          'position': this.destinationMarker.getPosition(),
          'title':  this.routeDistance
        });

        var ployLineCoords = this.convertCoords(response.routes[0].geometry.coordinates);
        this.map1.addPolylineSync({
          points: ployLineCoords,
          color: '#AA00FF',
          width: 2
        });
        this.map1.animateCamera({
          'target': ployLineCoords,
          'zoom': 17,
          'duration' : 200
        });
      })
    }
    
  }


  convertCoords(coords:Array<any>){
    return coords.map((element:Array<any>) => {
      return {lat : element[1], lng : element[0]}
    })
  }

  async search(event) {
    this.loading = await this.loadingCtrl.create({
      message: 'Please wait...'
    });
    await this.loading.present();
    this.map1.clear();

    // Address -> latitude,longitude
    Geocoder.geocode({
      "address": (this.search_address as any).value
    })
    .then((results: GeocoderResult[]) => {
      console.log(results);
      this.loading.dismiss();

      if (results.length > 0) {

        this.addSourceMarker(this.sourceLat,this.sourceLng);

        this.destinationMarker = this.map1.addMarkerSync({
          'position': results[0].position,
          'title':  'الوجهة'
        });
        this.destinationMarker.showInfoWindow();
        this.drawRoute();
      } else {
        alert("Not found");
      }
    });
  }

  saveDestLocation(){
    if(this.sourceMarker && this.destinationMarker){
        this.storage.set('locationSrc',JSON.stringify(this.sourceMarker.getPosition()));
        this.storage.set('locationDest',JSON.stringify(this.destinationMarker.getPosition()));
        
        //get src and dest coordinates
        let navigationOptions : NavigationOptions = {queryParams : {srcLat: this.sourceLat, srcLng: this.sourceLng , destLat : this.destinationMarker.getPosition().lat, destLng : this.destinationMarker.getPosition().lng , distance : this.routeDistance}}
        this.navCtrl.navigateBack('/student-request', navigationOptions);
      };
    }

  close(){
    this.navCtrl.pop();
  }
}
