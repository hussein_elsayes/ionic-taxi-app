import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
@Component({
  selector: 'app-family-request',
  templateUrl: './family-request.page.html',
  styleUrls: ['./family-request.page.scss'],
})
export class FamilyRequestPage implements OnInit {

  constructor( private fb: FormBuilder) { }
  
  familyRequestForm = this.fb.group(
    {
      name : ['',[Validators.required]],
      mobile_no : ['',[Validators.required]],
      email : ['',[Validators.required]],
      destination : ['',[Validators.required]]
    }
  );
  ngOnInit() {
  }
  onSubmit(familyRequestForm){
    
  }

}
