import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FamilyRequestPage } from './family-request.page';

describe('FamilyRequestPage', () => {
  let component: FamilyRequestPage;
  let fixture: ComponentFixture<FamilyRequestPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FamilyRequestPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FamilyRequestPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
