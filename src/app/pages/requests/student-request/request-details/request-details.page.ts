import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { LoadingController, AlertController, NavParams } from '@ionic/angular';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { HTTP } from '@ionic-native/http/ngx';
import { StudentRequestModel } from 'src/app/interfaces/StudentRequestModel';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-request-details',
  templateUrl: './request-details.page.html',
  styleUrls: ['./request-details.page.scss'],
})
export class RequestDetailsPage implements OnInit {
  appUrl = "http://173.212.247.166:8080/api";
  constructor(private _HTTP : HTTP,private activatedRoute : ActivatedRoute,
     private loadingCtrl : LoadingController,
      private authService : AuthenticationService,
      private alertCtrl : AlertController
      )
   {
      this.activatedRoute.queryParams.subscribe(returnedParams => {
        this.reqId = returnedParams['id'];
        this.getRequestDetails(this.reqId)
      })
  }

  reqId;
  selectedReq : StudentRequestModel;
  ngOnInit() {
   }

   ionViewDidEnter() {

  }

   getRequestDetails(reqId){
    this.loadingCtrl.create({message : "يرجى الانتظار"}).then(loadingEl => {
      loadingEl.present();

      this.authService.getToken().then(storefTokenObject => {
        let this_token = JSON.parse(storefTokenObject.data).token.toString();
        let bearerToken : string = 'Bearer ' + this_token.toString();
        console.log(bearerToken);
        let headers = {
          'Content-Type': 'application/json',
          'Authorization' : bearerToken
        }
        this._HTTP.setDataSerializer('json');
        this._HTTP.get(this.appUrl + '/requests/single/' + reqId,{}, headers).then((resp :any) => {
          loadingEl.dismiss();
          this.selectedReq = JSON.parse(resp.data);
          

        }).catch((error : HttpErrorResponse) => {
          loadingEl.dismiss();
          if(error.status == 401 || error.status ==403){
            this.authService.logout();
          }else
          {
            this.alertCtrl.create({message : "خطأ في الحصول على البيانات .. يرجى المحاولة لاحقا"}).then(returnedEl => {
              returnedEl.present();
            })
          }
        })
      })


      })
   }

}
