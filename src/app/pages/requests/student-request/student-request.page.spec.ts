import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentRequestPage } from './student-request.page';

describe('StudentRequestPage', () => {
  let component: StudentRequestPage;
  let fixture: ComponentFixture<StudentRequestPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StudentRequestPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StudentRequestPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
