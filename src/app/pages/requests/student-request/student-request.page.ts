import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { FormBuilder, Validators} from '@angular/forms';
import { NavController, LoadingController, AlertController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { ActivatedRoute, Router, Event, NavigationEnd, Params } from '@angular/router';
import { RequestsService } from 'src/app/services/requests.service';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { HTTP } from '@ionic-native/http/ngx';
import { HttpErrorResponse } from '@angular/common/http';
import { from } from 'rxjs';
import { finalize } from 'rxjs/operators';
@Component({
  selector: 'app-student-request',
  templateUrl: './student-request.page.html',
  styleUrls: ['./student-request.page.scss'],
})
export class StudentRequestPage implements OnInit{
  currentDate = new Date().toISOString();
  studentRequestForm = this.fb.group(
    {
      nationalId : ['',[Validators.required,Validators.pattern(/^[1-2][0-9]{9}$/)]],
      destinationDescription : ['',[Validators.required]],
      leaveTime : [this.currentDate,[Validators.required]],
      returnTime : [this.currentDate,[Validators.required]],
      roundTrip : [false,[Validators.required]],
      subscriptionType : ['day',[Validators.required]],
      startDate : [this.currentDate,[Validators.required]],
      fatherMobNo : ['',[Validators.required,Validators.pattern(/^[5][0-9]{8}$/)]],
      fatherNationalId : ['',[Validators.required,Validators.pattern(/^[1-2][0-9]{9}$/)]],
      locationSrcLat : [null,Validators.required],
      locationSrcLng : [null,Validators.required],
      locationDestLat : [null,Validators.required],
      locationDestLng : [null,Validators.required],
      routeDistance : [null,Validators.required]
    }
  );

  constructor(private fb: FormBuilder,private alertCtrl : AlertController,private _HTTP : HTTP,private authService : AuthenticationService,private loadingCtrl : LoadingController,private navCtrl : NavController,private storage : Storage,private activatedRoute : ActivatedRoute ,private router: Router, private requestService : RequestsService){ 
    this.activatedRoute.queryParams.subscribe(
      (params:Params)=>{
        console.log('logging query parameter destLat ...')
        console.log(params['destLat']);
        this.studentRequestForm.get('locationSrcLat').setValue(params['srcLat']);
        this.studentRequestForm.get('locationSrcLng').setValue(params['srcLng']);
        this.studentRequestForm.get('locationDestLat').setValue(params['destLat']);
        this.studentRequestForm.get('locationDestLng').setValue(params['destLng']);
        this.studentRequestForm.get('routeDistance').setValue(params['distance']);
      }
    )
  }



  appUrl = "http://173.212.247.166:8080/api";

  ngOnInit() {}

  async onSubmit(studentRequestForm){
    let loading = await this.loadingCtrl.create({message : "يرجى الانتظار"});
    await loading.present();
    console.log(studentRequestForm.value);
    from(this.requestService.createStudentRequest(studentRequestForm.value)).pipe(finalize(()=> {
      loading.dismiss();
    })).subscribe(resp=> {
        this.alertCtrl.create({message : "تم ارسال الطلب بنجاح"}).then(returnedEl => {
        returnedEl.present();})
        this.navCtrl.navigateRoot('/home');
    },(error : HttpErrorResponse)=> {
        if(error.status == 401 || error.status ==403){
          this.authService.logout();
        }else
        {
          this.alertCtrl.create({message : "خطأ في ارسال الطلب .. يرجى المحاولة لاحقا"}).then(returnedEl => {
            returnedEl.present();
          })
        }
    });

/*
    this.loadingCtrl.create({message : "يرجى الانتظار"}).then(loadingEl => {
      loadingEl.present();

      this.authService.getToken().then(storefTokenObject => {
        let this_token = JSON.parse(storefTokenObject.data).token.toString();

        let bearerToken : string = 'Bearer ' + this_token.toString();
        console.log(bearerToken);
        let headers = {
          'Content-Type': 'application/json',
          'Authorization' : bearerToken
        }
        this._HTTP.setDataSerializer('json');
        this._HTTP.post(this.appUrl + '/requests/student',studentRequestForm.value, headers).then((resp :any) => {
          loadingEl.dismiss();
          console.log(resp);
          this.alertCtrl.create({message : "تم ارسال الطلب بنجاح"}).then(returnedEl => {
            returnedEl.present();
          })
          this.navCtrl.navigateRoot('/home');
          //location.reload();
        }).catch((error : HttpErrorResponse) => {
          loadingEl.dismiss();
          console.log(error);
          if(error.status == 401 || error.status ==403){
            this.authService.logout();
          }else
          {
            this.alertCtrl.create({message : "خطأ في ارسال الطلب .. يرجى المحاولة لاحقا"}).then(returnedEl => {
              returnedEl.present();
            })
          }
        })
      })
    }) 
    */ 
  }

  close(){
    this.navCtrl.pop();
  }

}
