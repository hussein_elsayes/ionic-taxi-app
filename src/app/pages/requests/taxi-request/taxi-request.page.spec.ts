import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaxiRequestPage } from './taxi-request.page';

describe('TaxiRequestPage', () => {
  let component: TaxiRequestPage;
  let fixture: ComponentFixture<TaxiRequestPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaxiRequestPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaxiRequestPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
