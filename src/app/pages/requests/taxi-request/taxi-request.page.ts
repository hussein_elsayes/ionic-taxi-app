import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, ControlContainer } from '@angular/forms';

@Component({
  selector: 'app-taxi-request',
  templateUrl: './taxi-request.page.html',
  styleUrls: ['./taxi-request.page.scss'],
})
export class TaxiRequestPage implements OnInit {
  currentDate = new Date().toISOString();
  constructor( private fb: FormBuilder) { }

  taxiRequestForm = this.fb.group(
    {
      name : ['',[Validators.required]],
      mobile_no : ['',[Validators.required]],
      email : ['',[Validators.required]],
      destination : ['',[Validators.required]],
      time_interval : ['',[Validators.required]]
    }
  );
  ngOnInit() {
  }

  onSubmit(taxiRequestForm){
    console.log(taxiRequestForm.value);
  }

}
