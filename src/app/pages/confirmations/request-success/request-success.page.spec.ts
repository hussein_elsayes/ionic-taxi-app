import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestSuccessPage } from './request-success.page';

describe('RequestSuccessPage', () => {
  let component: RequestSuccessPage;
  let fixture: ComponentFixture<RequestSuccessPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequestSuccessPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestSuccessPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
