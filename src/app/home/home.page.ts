import { Component, OnInit } from '@angular/core';
import { RequestsService } from '../services/requests.service';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { StudentRequestModel } from '../interfaces/StudentRequestModel';
import { HTTP } from '@ionic-native/http/ngx';
import { AuthenticationService } from '../services/authentication.service';
import { AlertController, LoadingController, NavController } from '@ionic/angular';
import { NavigationOptions } from '@ionic/angular/dist/providers/nav-controller';
import { from } from 'rxjs';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit{

  public items: StudentRequestModel[];
  constructor(private navCtrl:NavController, private _requestService: RequestsService, private _HTTP : HTTP,  private authService : AuthenticationService,private alertCtrl : AlertController, private loadingCtrl : LoadingController) {}

  ngOnInit() {}

  ionViewDidEnter() {
    this.getRequestList();
  }

  async getRequestList(){
    let loading = await this.loadingCtrl.create({message : "يرجى الانتظار"});
    await loading.present();
    from(this._requestService.getStudentRequests()).pipe(finalize(()=> {
      loading.dismiss();
    })).subscribe((resp :any) => {
      this.items = <StudentRequestModel[]>JSON.parse(resp.data);
    }, (error:HttpErrorResponse) => {
      console.log(error);
      if(error.status == 401 || error.status ==403){
        this.authService.logout();
      }else
      {
        this.alertCtrl.create({message : "خطأ في الحصول على البيانات .. يرجى المحاولة لاحقا"}).then(returnedEl => {
          returnedEl.present();
        })
      }
    })
  }

  goToDetails(reqObj){
    let requestModel : StudentRequestModel = reqObj;
    let navigationOptions : NavigationOptions = {queryParams : {id: requestModel.reqId.toString()}}
    this.navCtrl.navigateForward('/student-request-details', navigationOptions);
  }


}
