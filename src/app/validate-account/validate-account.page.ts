import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { NavController, LoadingController, AlertController } from '@ionic/angular';
import { AuthenticationService } from '../services/authentication.service';
import { Router } from '@angular/router';
import { RequestsService } from '../services/requests.service';
import { HttpErrorResponse } from '@angular/common/http';
@Component({
  selector: 'app-validate-account',
  templateUrl: './validate-account.page.html',
  styleUrls: ['./validate-account.page.scss'],
})
export class ValidateAccountPage implements OnInit {

  constructor( private fb: FormBuilder,
    private router: Router,
    private loadingCtrl : LoadingController,
    private authService : AuthenticationService ,
    private alertCtrl : AlertController
    , private navCtrl : NavController,
    private requestService : RequestsService) { }


    validateForm = this.fb.group(
      {
        token : ['',[Validators.required]],
      }
    );

  ngOnInit() {
  }

  onSubmit(validateForm){
  console.log(validateForm);
  this.loadingCtrl.create({message : 'يرجى الانتظار '}).then(returnedElement => {
    returnedElement.present();
    this.requestService.validateUserAccount(this.validateForm.get('token').value).subscribe(resp => {
      //console.log(resp.data);
      returnedElement.dismiss()
      this.alertCtrl.create({message : "تم تفعيل المستخدم بنجاح"}).then(alertElement => {
        alertElement.present();
      })
      this.router.navigateByUrl('/login');
    }, (error : HttpErrorResponse) => {
      returnedElement.dismiss();
      console.log(error);
      if(error.status == 409){

        this.alertCtrl.create({message : "تم تفعيل المستخدم مسبقا"}).then(alertElement => {
          alertElement.present();
        })
      }else{

        this.alertCtrl.create({message : "خطأ في تفعيل المستخدم , يرجى المحاولة لاحقا"}).then(alertElement => {
          alertElement.present();
        })
      }

    });
  })

  }

  close(){
    this.navCtrl.pop();
  }

}
