import { LatLng } from '@ionic-native/google-maps';

export class Georoute {
    constructor(public length : number , public coordinates : Array<LatLng>){}
}