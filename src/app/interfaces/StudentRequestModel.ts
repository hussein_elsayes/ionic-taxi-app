export class StudentRequestModel {
    constructor(
        public reqId : string,
        public created_at:Date,
        public nationalId : string ,
        public leaveTime :Date,
        public returnTime : Date,
        public roundTrip :boolean,
        public subscriptionType : string,
        public startDate:Date,
        public fatherMobNo:string,
        public fatherNationalId:string,
        public locationSrcLat:string,
        public locationSrcLng:string,
        public locationDestLat:string,
        public locationDestLng:string,
        public destinationDescription:string,
        public routeDistance:string,
        public approved:boolean,
        public totalPrice:number
        ){}
}
