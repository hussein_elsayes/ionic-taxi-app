import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './guards/auth.guard';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then(m => m.HomePageModule), canLoad : [AuthGuard]
  },
  {
    path: 'list',
    loadChildren: () => import('./list/list.module').then(m => m.ListPageModule)
  },
  { path: 'student-request', loadChildren: './pages/requests/student-request/student-request.module#StudentRequestPageModule' , canLoad : [AuthGuard]},
  { path: 'family-request', loadChildren: './pages/requests/family-request/family-request.module#FamilyRequestPageModule' , canLoad : [AuthGuard]},
  { path: 'taxi-request', loadChildren: './pages/requests/taxi-request/taxi-request.module#TaxiRequestPageModule' , canLoad : [AuthGuard]},
  { path: 'login', loadChildren: './login/login.module#LoginPageModule' },
  { path: 'destination-location', loadChildren: './pages/shared/destination-location/destination-location.module#DestinationLocationPageModule' , canLoad : [AuthGuard]},
  { path: 'request-success', loadChildren: './pages/confirmations/request-success/request-success.module#RequestSuccessPageModule' },
  { path: 'signup', loadChildren: './signup/signup.module#SignupPageModule' },
  { path: 'validate-account', loadChildren: './validate-account/validate-account.module#ValidateAccountPageModule' },
  { path: 'student-request-details', loadChildren: './pages/requests/student-request/request-details/request-details.module#RequestDetailsPageModule' },
  { path: 'my-requests', loadChildren: './pages/requests/my-requests/my-requests.module#MyRequestsPageModule' },
  { path: 'admin', loadChildren: './admin/admin.module#AdminPageModule' }



];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
