import { Injectable } from '@angular/core';
import { HTTP } from '@ionic-native/http/ngx';
import { from, Observable } from 'rxjs';
import { AuthenticationService } from './authentication.service';
import { HttpResponse, HttpClient } from '@angular/common/http';
import { StudentRequestModel } from '../interfaces/StudentRequestModel';
import { Platform, LoadingController } from '@ionic/angular';
import { finalize } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class RequestsService {

  appUrl = "http://adiyattransport.com:8080/api";
  constructor(private _HTTP : HTTP, private authService : AuthenticationService,private http: HttpClient ,private plt: Platform) { }

   async getStudentRequests(){

    if(this.plt.is('cordova')){
      return this.getStudentRequestsNative();
    }else
    {
      return this.getStudentRequestsStandard();
    }
  }


  private async getStudentRequestsNative(){
    let token = await this.authService.getToken();
    token = JSON.parse(token.data).token.toString();
    let headers = {'Content-Type': 'application/json','Authorization' : 'Bearer ' + token.toString()}
    this._HTTP.setDataSerializer('json');
    return this._HTTP.get(this.appUrl + '/requests/get',{}, headers);
  }

  private async getStudentRequestsStandard()
  {
    let token = await this.authService.getToken();
    token = JSON.parse(token.data).token.toString();
    let headers = {'Content-Type': 'application/json','Authorization' : 'Bearer ' + token.toString()};
    this.http.get(this.appUrl + '/requests/get',{headers : headers});
  }

  createStudentRequest(studentForm){
    if(this.plt.is('cordova')){
      return this.createStudentRequestNative(studentForm);
    }else
    {
      return this.createStudentRequestStandard(studentForm);
    }
  }

  private async createStudentRequestNative(studentForm){
    let token = await this.authService.getToken();
    token = JSON.parse(token.data).token.toString();
    let headers = {'Content-Type': 'application/json','Authorization' : 'Bearer ' + token.toString()}
    this._HTTP.setDataSerializer('json');
    return this._HTTP.post(this.appUrl + '/requests/student',studentForm, headers);
  }

  private async createStudentRequestStandard(studentForm){
    let token = await this.authService.getToken();
    token = JSON.parse(token.data).token.toString();
    let headers = {'Content-Type': 'application/json','Authorization' : 'Bearer ' + token.toString()};
    return this.http.post(this.appUrl + '/requests/student',studentForm,{headers : headers});
  }

  createUserRequest(signupForm){
    if(this.plt.is('cordova')){
      return this.createUserRequestNative(signupForm);
    }else
    {
      return this.createUserRequestStandard(signupForm);
    }
  }

  createUserRequestNative(signupForm){
    this._HTTP.setDataSerializer('json');
    return from(this._HTTP.post(this.appUrl + '/createUser',signupForm, {'Content-Type': 'application/json'}))
  }

  createUserRequestStandard(signupForm){
    return this.http.post(this.appUrl + '/requests/student',signupForm);
  }

  validateUserAccount(token){
    if(this.plt.is('cordova')){
      return this.validateUserAccountNative(token);
    }else
    {
      return this.validateUserAccountStandard(token);
    }
  }

  validateUserAccountNative(token){
    this._HTTP.setDataSerializer('json');
    return from(this._HTTP.get(this.appUrl + '/validateUser/'+ token,{}, {'Content-Type': 'application/json'}))
  }

  validateUserAccountStandard(token){
    return this.http.get(this.appUrl + '/validateUser/'+ token);
  }
}
