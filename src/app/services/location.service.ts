import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {LatLng} from '@ionic-native/google-maps';
import { Georoute } from '../interfaces/GeoRoute';
import { resetFakeAsyncZone } from '@angular/core/testing';
import { reject } from 'q';

@Injectable({
  providedIn: 'root'
})
export class LocationService {

  constructor(private _http:HttpClient) { }

    access_token = 'pk.eyJ1IjoiaHVzc2VpbmVtYW0iLCJhIjoiY2p6NHhtemxkMGhtdDNjcXQwYmh3Mm54eCJ9.zd9myf_bp8VLYu11IMrDAQ';

    getGeoRoute(src,dest){
      console.log('from service : gotten src ...')
      console.log(src.lng);

      var routeDistance;
      var rootCoordinates; 
      return this._http.get('https://api.mapbox.com/directions/v5/mapbox/driving/'+src.lng+','+src.lat+';'+dest.lng+','+dest.lat+'?geometries=geojson&access_token='+this.access_token);
    }

}
