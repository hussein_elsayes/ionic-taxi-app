import { Injectable } from '@angular/core';
import { HTTP } from '@ionic-native/http/ngx';
import { from } from 'rxjs';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';
import { Platform } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  isAuthenticated =false;

  appUrl = "http://173.212.247.166:8080/api";
  constructor(private http: HttpClient, private _HTTP : HTTP,private storage : Storage,private router: Router,private plt: Platform) { }

  login(logincredentials){
    this.isAuthenticated = true;
    if(this.plt.is('cordova')){
      return this.loginNative(logincredentials);
    }else
    {
      return this.loginStandard(logincredentials);
    }
  }

  loginStandard(logincredentials){
    return this.http.post(this.appUrl + '/login',logincredentials);
  }

  loginNative(logincredentials){
    this._HTTP.setDataSerializer('json');
    let observable = from(this._HTTP.post(this.appUrl + '/login',logincredentials, {'Content-Type': 'application/json'}))
    return observable;
  }

  logout(){
    this.storage.remove('authToken');
    this.router.navigateByUrl('/login');
    this.isAuthenticated = false;
  }

  loggedIn()
  {
    /*this.storage.get('authToken').then(returnedToken => {
      if(returnedToken){
        return true;
      }
    })
    return false;*/
    return this.isAuthenticated;
  }

  getToken(){
    return this.storage.get("authToken");
  }
}
