import { Component, OnInit } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AuthenticationService } from './services/authentication.service';
import { Router } from '@angular/router';
import { TokenModel } from './interfaces/TokenModel';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent implements OnInit{
  
  email;
  
  public appPages = [
    {
      title: 'الرئيسية',
      url: '/home',
      icon: 'home'
    },
    {
      title: 'نقل جامعي',
      url: '/student-request',
      icon: 'bus'
    },
    {
      title : 'نقل عائلي',
      url : '/family-request',
      icon : 'contacts'
    },
    {
      title : 'استئجار سيارة بسائق',
      url : '/taxi-request',
      icon : 'car'
    }
  ];

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private authService : AuthenticationService,
    private router:Router
  ) {
    this.initializeApp();
  }

  ngOnInit(){
    this.authService.getToken().then(returnedToken =>{
      this.email = JSON.parse(returnedToken.data).userEmail;
    }).catch(error => {
      this.authService.logout();
    })
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

ng

  logout(){
    this.authService.logout();
    this.router.navigateByUrl('/login');
  }

  getUserName(){
    
  }
}
