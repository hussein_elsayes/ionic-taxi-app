import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { AdminPage } from './admin.page';
import { DriversComponent } from './drivers/drivers.component';

const routes: Routes = [
  {
    path: '',
    component: AdminPage
  },
  {
    path : 'drivers',
    component : DriversComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [AdminPage , DriversComponent]
})
export class AdminPageModule {}
